Ready to get install your Theme? First, you need to download.

1. Access your account at <a href="http://bindtuning.com" target="_blank">Bind Tuning</a>;
2. Go to **My Downloads**, switch to the **Themes** tab.
3. Mouse hover the theme and click **View theme** to open the Web Part details page;

    ![theme-select.png](..\images\theme-select.png)

4. Last but not least, click on **Download**.

    ![download-theme](..\images\download-theme.png)