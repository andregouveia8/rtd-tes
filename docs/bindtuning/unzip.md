The themes are provided in 2 different packaging all following Microsoft guidelines:

For **Classic Experience**: 

 - ***WSP***, no-code sandbox solution.

For **Modern Experience**: 

 - ***SPFx***, Modern SharePoint.

After unzipping your Theme package, you will find two folders, a **THEME_NAME.SPO2013.zip**, and **THEME_NAME.SPFx.zip**. You will be using **THEME_NAME.SPFx.zip** to install the Modern Experience theme.

![zip.png](../images/zip.png)

-------------
**SPFx** (Modern SharePoint) 📁

![themestructure.png](../images/themestructure.png)

- THEME_NAME.json
- THEME_NAME.spcolor
- THEME_NAME.spfx.sppkg
- Installer.ps1

---------

**SPO2013** (Classic SharePoint) 📁

└──  THEME_NAME.SPO2013.wsp

<p class="alert alert-info">To install this theme version go to <a href="https://bindtuning-office-365-themes.readthedocs.io/en/latest/" target="_blank">SharePoint Office 365 </a>.</p>