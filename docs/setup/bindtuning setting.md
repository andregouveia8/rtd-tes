With the BindTuning SPFx Themes you can decide how the page is affected by the theme. To do this follow these steps:


1. Click **Edit** on the top right corner of the page content - this will put the page on edit mode;

	<p class="alert alert-success">Alternatively, if you’re on a library page, you can open up your browser’s inspector by pressing F12 and on the console and typing <strong>“btOpenSettings()”</strong>.</p>
	
2. On the command bar click on **BindTuning Settings** option;
3. A settings pane will open from the left - this gives you access to the optional theme properties:

	- [Favicon](#favicon)
	- [Compact Styling](#styling)
	- [Hide Page Title](#hidepagetitle)
	- [Hide Footer](#hidefooter)
	- [Hide Breadcrumb](#breadcrumb)
	- [Headings](#headings)
	- [Show Feedback Button](#feedbackbutton)
	- [Custom CSS](#customcss)
	- [Export Settings](#export)
	- [Import Settings](#import)


![14-bindtuning-settings.png](../images/15-bindtuning-settings.png)

---
<a name="favicon"></a>
### Favicon 

Sets a custom Favicon for your site collection. 

<p class="alert alert-info"><b>Not sure what a Favicon is?</b> <br>
Favicon is an icon that is picked up from the website to accompany the site name in the browser tabs and the Bookmarks/Favorites bar.</p>

![16-favicon.png](../images/16.favicon.png)

---
<a name="styling"></a>
### Compact Styling

This will reduce the spacing between elements like the logo and navbar, paddings in the footer and header and possibly other elements. It works to increase the available content area.

---
<a name="hidepagetitle"></a>
### Hide Page Title
Hides theme’s page title.

---
<a name="hidefooter"></a>
### Hide Footer

Allows you to hide the footer, increasing the available content area. 

---
<a name="breadcrumb"></a>
### Hide Breadcrumb

Hide Breadcrumb from the SharePoint site.

---
<a name="headings"></a>
### Headings

Apply theme heading typefaces to SPFx content and web parts. 

---
<a name="feedbackbutton"></a>
### Show Feedback Button

Hide SharePoint’s Modern Pages feedback button;

---

<a name="customcss"></a>
### Custom CSS
Allows you to add your custom CSS to the theme.

---

<a name="export"></a>
### Export Settings

To export your settings and content open the settings panel and click **Export**. This will generate a JSON file that you can select when prompted.
	
![export-btsettings.png](../images/export-btsettings.png)

----
<a name="import"></a>
### Import Settings

1. To import your settings and content open the settings panel and click **import**. You will be prompted to select a file. Select the JSON file you’ve exported.


2. Click **Confirm** when the confirmation modal shows up and you’re done!
	
![import btsettings](../images/import-btsettings.png)